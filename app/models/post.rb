class Post < ActiveRecord::Base
	#before comma states relationship with comments model
	#after comma states that it will destroy any comments associated with post being deleted
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body
end
